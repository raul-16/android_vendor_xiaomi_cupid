#
# Automatically generated file. DO NOT MODIFY
#

LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),cupid)

$(call add-radio-file-sha1-checked,radio/abl.img,4bfbc8d7991f92ef651d8eeda5bce1037d6e1c62)
$(call add-radio-file-sha1-checked,radio/aop.img,9fba7070214d8e1ca5f4e7e4fdee1bcdf32ff95f)
$(call add-radio-file-sha1-checked,radio/aop_config.img,e5f84f04965baa04671fcbeff6c8e3692e47bfc3)
$(call add-radio-file-sha1-checked,radio/bluetooth.img,a38b6bf55ec4ce5f652df6f0771222a670a542ea)
$(call add-radio-file-sha1-checked,radio/cpucp.img,c0b15cbad782656df90c0b532af0d9d01734a09c)
$(call add-radio-file-sha1-checked,radio/devcfg.img,fbc33b26905de6867b65d80d045e1236a1953853)
$(call add-radio-file-sha1-checked,radio/dsp.img,fc3c899ed45347bcf455835448574032c58d6fba)
$(call add-radio-file-sha1-checked,radio/featenabler.img,4d0543cc1023085d124c1235156ed1b53682d4e9)
$(call add-radio-file-sha1-checked,radio/hyp.img,2b3357cc378fc00645109903be92c4bb815ac432)
$(call add-radio-file-sha1-checked,radio/imagefv.img,9df338b63514219ca9ce630dacc36b19c4359e9f)
$(call add-radio-file-sha1-checked,radio/keymaster.img,e4e266f3bf81aff4596add97f6ef24f46c1501cb)
$(call add-radio-file-sha1-checked,radio/modem.img,b76b69033b68f4ab84b49a89e7a7775c94c81ee9)
$(call add-radio-file-sha1-checked,radio/qupfw.img,d2f7015145abf9c9b4eb321384198edb87618a6b)
$(call add-radio-file-sha1-checked,radio/shrm.img,275f647085b65e75bce3e8e540e6e387109191f6)
$(call add-radio-file-sha1-checked,radio/tz.img,0927c61bec79d990ec44f5c0b86b04e5164fed2a)
$(call add-radio-file-sha1-checked,radio/uefi.img,84be926fcf04b97f984f3430c0e97937f83d1cdb)
$(call add-radio-file-sha1-checked,radio/uefisecapp.img,1e026fe1b34e1130f801ae741d8ed374916b3c6f)
$(call add-radio-file-sha1-checked,radio/xbl.img,3d4396843e09e73dc0ee1041e536965bb79157ee)
$(call add-radio-file-sha1-checked,radio/xbl_config.img,04696f58b93447fa98475eb61a301406d793bad9)
$(call add-radio-file-sha1-checked,radio/xbl_ramdump.img,1f64107ac7ff607b0280e8f2e0a2146b4c6987cf)

endif
